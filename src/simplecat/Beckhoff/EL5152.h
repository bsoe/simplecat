/**
 * This file is part of SimpleECAT.
 *
 * SimpleECAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SimplECAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with SimpleECAT.  If not, see <http://www.gnu.org/licenses/>.
 *
 * \class Beckhoff_EL5152
 *
 * \ingroup SimplECAT
 *
 * \brief Beckhoff EL5152
 *
 * 1 Quadrature Incremental encoder input
 *
 * https://download.beckhoff.com/download/document/io/ethercat-terminals/el5101en.pdf
 */


#ifndef SIMPLECAT_BECKHOFF_EL5152_H_
#define SIMPLECAT_BECKHOFF_EL5152_H_

#include <simplecat/Slave.h>
#include <iostream>

namespace simplecat {


class Beckhoff_EL5152 : public Slave
{

public:
    Beckhoff_EL5152() : Slave(0x00000002, 0x14203052) {}
    virtual ~Beckhoff_EL5152() {}

    virtual void processData(size_t index, uint8_t* domain_address){
        if (index < 2) {
            read_value[index] = EC_READ_U32(domain_address);    
        }
        // switch (index % 2) {
        //     case 0: // counter value
        //         read_value[index/2] = EC_READ_U32(domain_address);
        //         break;
        //     // case 1: // frequency
        //     //     read_frequency[index/2] = EC_READ_U32(domain_address);
        //     //     break;
        // }
    }

    virtual const ec_sync_info_t* syncs() { return &syncs_[0]; }

    virtual size_t syncSize() {
        return sizeof(syncs_)/sizeof(ec_sync_info_t);
    }

    virtual const ec_pdo_entry_info_t* channels() {
        return channels_;
    }

    virtual void domains(DomainMap& domains) const {
        domains = domains_;
    }

    // read: encoder counts
    uint32_t read_value [2]  = {0};
    // read: encoder frequency
    uint32_t read_frequency [2] = {0};

private:
    ec_pdo_entry_info_t channels_[40] = {
        // write
        {0x0000, 0x00, 1}, /* Gap */
        {0x0000, 0x00, 1}, /* Gap */
        {0x7000, 0x03, 1}, /* Counter set */
        {0x0000, 0x00, 1}, /* Gap */
        {0x0000, 0x00, 4}, /* Gap */
        {0x0000, 0x00, 8}, /* Gap */
        {0x7000, 0x11, 32}, /* Counter Value to set */
        // read
        {0x0000, 0x00, 2}, /* Gap */
        {0x6000, 0x03, 1}, /* Set counter done */
        {0x0000, 0x00, 4}, /* Gap */
        {0x6000, 0x08, 1}, /* Extrapolation stall - for micro increments only */
        {0x6000, 0x09, 1}, /* Input A status */
        {0x6000, 0x0a, 1}, /* Input B status */
        {0x0000, 0x00, 3}, /* Gap */
        {0x6000, 0x0e, 1}, /* Sync error - for DC mode only */
        {0x0000, 0x00, 1}, /* Gap */
        {0x6000, 0x10, 1}, /* TxPDO toggle - 1 if associated TxPDO is updated */
        {0x6000, 0x011, 32}, /* Counter Value */
        // read frequency
        {0x6000, 0x013, 32}, /* Frequency Value */
        // read period
        {0x6000, 0x014, 32}, /* Period Value */
        // write
        {0x0000, 0x00, 1}, /* Gap */
        {0x0000, 0x00, 1}, /* Gap */
        {0x7010, 0x03, 1}, /* Counter set */
        {0x0000, 0x00, 1}, /* Gap */
        {0x0000, 0x00, 4}, /* Gap */
        {0x0000, 0x00, 8}, /* Gap */
        {0x7010, 0x11, 32}, /* Counter Value to set */
        // read
        {0x0000, 0x00, 2}, /* Gap */
        {0x6010, 0x03, 1}, /* Set counter done */
        {0x0000, 0x00, 4}, /* Gap */
        {0x6010, 0x08, 1}, /* Extrapolation stall - for micro increments only */
        {0x6010, 0x09, 1}, /* Input A status */
        {0x6010, 0x0a, 1}, /* Input B status */
        {0x0000, 0x00, 3}, /* Gap */
        {0x6010, 0x0e, 1}, /* Sync error - for DC mode only */
        {0x0000, 0x00, 1}, /* Gap */
        {0x6010, 0x10, 1}, /* TxPDO toggle - 1 if associated TxPDO is updated */
        {0x6010, 0x011, 32}, /* Counter Value */
        // read frequency
        {0x6010, 0x013, 32}, /* Frequency Value */
        // read period
        {0x6010, 0x014, 32}, /* Period Value */
    };

    ec_pdo_info_t pdos_[4] = {
        /* channel 1 - Rx */
        {0x1600, 7, channels_ + 0}, /* RxPDO-Map Outputs */
        /* channel 2 - Rx */
        {0x1602, 7, channels_ + 20}, /* RxPDO-Map Outputs */
        /* channel 1 - Tx */
        {0x1a00, 11, channels_ + 7}, /* TxPDO-Map Inputs */
        // {0x1a03, 1, channels_ + 18}, /* TxPDO-Map Inputs - Frequency */
        // {0x1a02, 1, channels_ + 19}, /* TxPDO-Map Inputs - Period */
        /* channel 2 - Tx */
        {0x1a04, 11, channels_ + 27}, /* TxPDO-Map Inputs */
        // {0x1a07, 1, channels_ + 38}, /* TxPDO-Map Inputs - Frequency */
        // {0x1a06, 1, channels_ + 39}, /* TxPDO-Map Inputs - Period */
    };

    ec_sync_info_t syncs_[5] = {
        {0, EC_DIR_OUTPUT, 0, NULL, EC_WD_DISABLE},     // mailbox Rx
        {1, EC_DIR_INPUT, 0, NULL, EC_WD_DISABLE},      // mailbox Tx
        {2, EC_DIR_OUTPUT, 2, pdos_, EC_WD_DISABLE},    // process data Rx
        {3, EC_DIR_INPUT, 2, pdos_ + 2, EC_WD_DISABLE},     // process data Tx
        {0xff}
    };

    DomainMap domains_ = {
        // {0, {17,18,33,34}} // currently, only read the counter and frequency value
        {0, {17,37, 2, 22}}
    };
};


}

#endif
