// EL3356: 1-channel precise load cell analysis
#ifndef EL3356_H
#define EL3356_H

//NOTE: this class does not handle setting calibration data. Ideally,
// this should be done on the twin cat system.

#include <simplecat/Slave.h>
#include <iostream>

namespace simplecat {


class Beckhoff_EL3356 : public Slave
{

public:
    Beckhoff_EL3356() : Slave(0x00000002, 0x0d1c3052) {}
    virtual ~Beckhoff_EL3356() {}

    virtual void processData(size_t index, uint8_t* domain_address){
        switch (index) {
            case 0:
            	//TODO: add logic to self calibrate at startup, then disable calibration
                break;
            case 1:
                read_value = EC_READ_S32(domain_address);
                break;
        }
    }

    virtual const ec_sync_info_t* syncs() { return &syncs_[0]; }

    virtual size_t syncSize() {
        return sizeof(syncs_)/sizeof(ec_sync_info_t);
    }

    virtual const ec_pdo_entry_info_t* channels() {
        return channels_;
    }

    virtual void domains(DomainMap& domains) const {
        domains = domains_;
    }

    // read value of the load cell
    int32_t read_value = 0;

private:
    ec_pdo_entry_info_t channels_[23] = {
        // write control
        {0x7000, 0x01, 1}, /* Start calibration */
        {0x7000, 0x02, 1}, /* Disable calibration if 1, activate if 0 */
        {0x7000, 0x03, 1}, /* Input freeze */
        {0x0000, 0x00, 1}, /* Gap */
        // {0x7000, 0x04, 1}, /* Sample mode: 0 = 10.5kHz high precision, 105kHz = low latency */
        // ^ for EL3356-0010 and EL3356-0090 only 
        {0x7000, 0x05, 1}, /* Tare. Offset is stored in volatile memory. */
    	{0x0000, 0x00, 3}, /* Gap */
    	{0x0000, 0x00, 8}, /* Gap */
    	// write filter frequency
    	{0x7000, 0x11, 16}, /* Filter frequency.*/
    	/*^ Valid range: 0 < f <= 2000. For all other values, behaves like 50Hz FIR filter.*/
        // read status
        {0x0000, 0x00, 1}, /* Gap */
        {0x6000, 0x02, 1}, /* Overrange */
        {0x0000, 0x00, 1}, /* Gap */
        {0x6000, 0x04, 1}, /* Data invalid */
        {0x0000, 0x00, 2}, /* Gap */
        {0x6000, 0x07, 1}, /* Error */
        {0x6000, 0x08, 1}, /* Calibration in progress */
        {0x6000, 0x09, 1}, /* Steady state */
        {0x0000, 0x00, 4}, /* Gap */
        {0x6000, 0x0e, 1}, /* Sync error - for distributed clock */
        {0x0000, 0x00, 1}, /* Gap */
        {0x6000, 0x10, 1}, /* TxPDO toggle */
        // read int32 value
        {0x6000, 0x11, 32}, /* Value in INT32 */
    	// read REAL32 value
        {0x6000, 0x12, 32}, /* Value in fixed point REAL32 */
        // read timestamp
        {0x6000, 0x13, 64}, /* Timestamp - for distributed clock */
    };

    ec_pdo_info_t pdos_[3] = {
        {0x1600, 7, channels_ + 0}, /* RxPDO-Map Outputs - Control */
        // {0x1601, 1, channels_ + 7}, /* RxPDO-Map Outputs - Filter frequency */
        {0x1a00, 12, channels_ + 8}, /* TxPDO-Map Inputs - Status */
    	{0x1a01, 1, channels_ + 20}, /* TxPDO-Map Inputs - Value INT32 */
    	// {0x1a02, 1, channels_ + 21}, /* TxPDO-Map Inputs - Value  */
    	// {0x1a03, 1, channels_ + 22}, /* TxPDO-Map Inputs - Timestamp */
    };

    ec_sync_info_t syncs_[3] = {
        // {0, EC_DIR_OUTPUT, 0, NULL, EC_WD_DISABLE},
        // {1, EC_DIR_INPUT, 0, NULL, EC_WD_DISABLE},
        {2, EC_DIR_OUTPUT, 1, pdos_ + 0, EC_WD_DISABLE},
        {3, EC_DIR_INPUT, 2, pdos_ + 1, EC_WD_DISABLE},
        {0xff}
    };

    DomainMap domains_ = {
        {0, {1,20}}
    };
};


}





#endif //EL3356_H